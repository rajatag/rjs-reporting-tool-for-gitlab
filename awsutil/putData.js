import getTableName from './getTableName';

// to put all data in dynamodb database
export default function (pid, projectName, ccname, dynamodb) {
    let docClient = new AWS.DynamoDB.DocumentClient();
    console.log("pid-put",pid);
    getTableName().then(tableName => {
        let params = {
            TableName: tableName,
            Item: {
                "pid": pid,
                "projectname": projectName,
                "ccname": ccname
            }
        };
        docClient.put(params, function (err, data) {
            if (err) {
                console.log("put error", err);
            } else {
                console.log("put succeed", data);
            }
        });
    });

}