import config from '../config';
import getTableName from './getTableName';
export default function(pid,dynamodb){
    let docClient = new AWS.DynamoDB.DocumentClient();
    getTableName().then(tableName=>{
        let params = {
            TableName: tableName,
            Key: {
                "pid": pid,
            }
        };
        docClient.get(params, function (err, data) {
            if (err) {
                console.log("get error",err);
            } else {
                console.log("get succeed",data);
            }   
        });
    });
}