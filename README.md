# Development Guide

After getting the source folder.
```npm install```

## For Development
```npm run start```
This will trigger parcel and generate the dist directory, also starts a server with Hot Module Reloading enabled on port 1234.

## For Deploy build
```npm run build```
This will build the project for production along with minification.

## Deploy
```firebase deploy```
This deploys the projet to firebase, make sure you are logged into the correct google account.

#### Top Level Compoents are in "container" and smaller ones are in "components"

## Tests
The testing suite is run by jest. It runs test on the filtering logic and most of the components.

The component tests are run as snapshot test.  A "snapshot" of the components are saved and every time the test suite is run it is compared to the current rendering of the component and if anything has changed the test fails.
To run the regular test (when a component change is not intended):

`npm run test`

If the changes are intended, run:

`npm run test:update`

The test for the logic would run the same in both cases.

To edit the mock data check the `mockData.js` file and the `mockNotes.js` file. 

**If, sometimes running `npm run start` produces an unexpected output try deleting the `.cache` folder and starting again.**

When running on linux the commands might run only with `sudo`.

## License

Reporting Tool For Gitlab is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Reporting Tool For Gitlab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Reporting Tool For Gitlab. If not, see http://www.gnu.org/licenses/.