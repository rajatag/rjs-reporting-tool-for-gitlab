import React from "react";
import ReactDOM from "react-dom";

import Nav from './containers/Nav';

var mountNode = document.getElementById("app");
ReactDOM.render(
    <Nav/>
    , mountNode);