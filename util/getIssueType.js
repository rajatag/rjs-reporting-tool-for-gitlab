export default function(issue){
    for(let label of issue.labels){
        if(label.toLocaleLowerCase()==='bug'){
            return 'bug';
        }
        if(label.toLocaleLowerCase()==='feature'){
            return 'feature';
        }
    }
}