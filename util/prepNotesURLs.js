import config from '../config'
export default function(mapping){
    let urls = [];
    for(let projectid in mapping){
        for(let issue of mapping[projectid].issues){
            urls.push({url:`${config.BASE}/projects/${projectid}/issues/${issue.iid}/notes?sort=desc&order_by=updated_at&per_page=100&page=1`,iid:issue.iid,projectid:projectid,id:issue.id});
        }    
    }
    return urls;
}