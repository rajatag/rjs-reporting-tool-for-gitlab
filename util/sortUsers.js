//sort users on the basis of names, because gitlab wont do it for us
export default function sortUsers(users){
    return users.sort((a, b) => {
        let nameA = a.name.toLowerCase(), nameB = b.name.toLowerCase()
        if (nameA < nameB) //sort string ascending
            return -1
        if (nameA > nameB)
            return 1
        return 0
    });
}