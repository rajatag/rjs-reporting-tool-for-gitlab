import notes from "./mockNotes";
import filterUserNotes from "../util/filterUserNotes";
import filterSpendNotes from "../util/filterSpendNotes";
import filterMonthNotes from "../util/filterMonthNotes";
import getDateData from "../util/getDateData";



test('filters notes by the current author', () => {
  let userNote1 = filterUserNotes(notes, 4);

  expect(userNote1).toHaveLength(2);

  let userNote2 = filterUserNotes(notes, 1);

  expect(userNote2).toHaveLength(3);
});

test('filters spend notes', () => {
  let spendNotes = filterSpendNotes(notes);
  expect(spendNotes).toHaveLength(6);

});

test('filters notes from given month', () => {
  let spendNotes = filterSpendNotes(notes);
  let monthNotes = filterMonthNotes(spendNotes, 5, 2018);

  expect(monthNotes).toHaveLength(2);
});

test('filters notes from given month', () => {
  let spendNotes = filterSpendNotes(notes);
  let monthNotes = filterMonthNotes(spendNotes, 5, 2018);

  expect(monthNotes).toHaveLength(2);
});

test('parses the notes to give correct working hours for a day', () => {
  let userNotes = filterUserNotes(notes, 9);
  let spendNotes =filterSpendNotes(userNotes);
  let monthNotes = filterMonthNotes(spendNotes, 7,2018);
  let dateData = getDateData(monthNotes);

  expect(dateData[30]).toBe(5);
});









