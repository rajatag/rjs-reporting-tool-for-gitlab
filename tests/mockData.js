let notesOne = [
  {
    id: 1000,
    type: null,
    body: "unassigned @jdoe",
    attachment: null,
    author: {
      id: 1,
      name: "John Doe",
      username: "jdoe",
      state: "active",
      avatar_url: "",
      web_url: ""
    },
    created_at: "2018-06-21T05:49:53.854Z",
    updated_at: "2018-06-21T05:49:53.854Z",
    system: true,
    noteable_id: 300,
    noteable_type: "Issue",
    resolvable: false,
    noteable_iid: 12
  }
];

let notesTwo = [
  {
    id: 1000,
    type: null,
    body: "unassigned @jdoe",
    attachment: null,
    author: {
      id: 1,
      name: "John Doe",
      username: "jdoe",
      state: "active",
      avatar_url: "",
      web_url: ""
    },
    created_at: "2018-06-21T05:49:53.854Z",
    updated_at: "2018-06-21T05:49:53.854Z",
    system: true,
    noteable_id: 300,
    noteable_type: "Issue",
    resolvable: false,
    noteable_iid: 12
  }
];

let fakeDateData1 = [];
fakeDateData1[0] = -1;
for (let i = 1; i <= 31; i++) {
  if (i == 20) {
    fakeDateData1.push(3);
  } else {
    fakeDateData1.push(0);
  }
}

let fakeDateData2 = [];
fakeDateData2[0] = -1;
for (let i = 1; i <= 31; i++) {
  if (i == 22) {
    fakeDateData1.push(1);
  } else {
    fakeDateData1.push(0);
  }
}

let fakeColorData = [];
fakeColorData[0] = null;
for (let i = 1; i <= 31; i++) {
  fakeColorData.push("#fff");
}

let issuesOne = {
  closedAt: null,
  colorData: fakeColorData,
  dateData: fakeDateData1,
  duedate: null,
  esitmate: "0",
  flag: true,
  id: 300,
  iid: 12,
  notes: [notesOne],
  openedAt: "2018-06-20T05:45:29.951Z",
  state: "opened",
  title: "some title",
  type: "feature",
  weburl: "example.com"
};

let issueTwo = {
  closedAt: "2018-06-25T14:39:04.809Z",
  colorData: fakeColorData,
  dateData: fakeDateData2,
  duedate: null,
  esitmate: "3",
  flag: true,
  id: 300,
  iid: 12,
  notes: [notesOne],
  openedAt: "2018-06-19T05:45:29.951Z",
  state: "opened",
  title: "some title",
  type: "feature",
  weburl: "example.com"
};

let fakeProjectDataOne = [];
fakeProjectDataOne[10] = { issues: [issuesOne] };

fakeProjectDataOne[11] = { issues: [issueTwo] };

let pnames = [];
pnames[10] = {
  name: "ProjectOne",
  name_with_namespace: "Test / ProjectOne",
  pid: undefined,
  url: "example.com"
};

pnames[11] = {
  name: "ProjectTwo",
  name_with_namespace: "Test / ProjectTwo",
  pid: undefined,
  url: "example.com"
};

let fakeDailyProps = {
  issuelist: undefined,
  base: "example.com",
  data: fakeProjectDataOne,
  date: 20,
  name: "John Doe",
  pkey: "_xfakekeyjndsjyTnlol",
  pnames: pnames
};

let fakeCCData = [];
fakeCCData[10] = "PROJ1";
fakeCCData[11] = "PROJ2";

let fakeUsers = [
  {
    id: 1,
    name: "John Doe",
    username: "jdoe",
    state: "active",
    avatar_url: "",
    web_url: "https://gitlab.example.com/jdoe",
    created_at: "2018-05-24T10:29:49.133Z",
    bio: null,
    location: null,
    skype: "",
    linkedin: "",
    twitter: "",
    website_url: "",
    organization: null,
    last_sign_in_at: "2018-06-22T04:23:25.514Z",
    confirmed_at: "2018-05-24T10:29:49.047Z",
    last_activity_on: null,
    email: "jdoe@example.com",
    theme_id: 1,
    color_scheme_id: 2,
    projects_limit: 10,
    current_sign_in_at: "2018-06-26T07:30:19.500Z",
    identities: [],
    can_create_group: false,
    can_create_project: true,
    two_factor_enabled: false,
    external: false,
    is_admin: false
  },
  {
    id: 2,
    name: "Tony Stark",
    username: "tstark",
    state: "active",
    avatar_url: "",
    web_url: "https://gitlab.example.com/tstark",
    created_at: "2018-05-17T10:09:44.585Z",
    bio: null,
    location: null,
    skype: "",
    linkedin: "",
    twitter: "",
    website_url: "",
    organization: null,
    last_sign_in_at: "2018-06-22T13:26:12.691Z",
    confirmed_at: "2018-05-17T10:09:44.166Z",
    last_activity_on: null,
    email: "tstark@example.com",
    theme_id: 1,
    color_scheme_id: 1,
    projects_limit: 10,
    current_sign_in_at: "2018-06-25T14:18:44.309Z",
    identities: [],
    can_create_group: false,
    can_create_project: true,
    two_factor_enabled: false,
    external: false,
    is_admin: false
  },
  {
    id: 3,
    name: "Severus Dumbledore",
    username: "sdumbledore",
    state: "active",
    avatar_url: "",
    web_url: "https://gitlab.example.com/sdumbledore",
    created_at: "2018-03-20T13:35:23.395Z",
    bio: null,
    location: null,
    skype: "",
    linkedin: "",
    twitter: "",
    website_url: "",
    organization: null,
    last_sign_in_at: "2018-06-20T14:29:11.606Z",
    confirmed_at: "2018-03-20T13:35:23.093Z",
    last_activity_on: "2018-03-20",
    email: "sdumbledore@example.com",
    theme_id: 1,
    color_scheme_id: 1,
    projects_limit: 10,
    current_sign_in_at: "2018-06-22T12:04:18.493Z",
    identities: [],
    can_create_group: false,
    can_create_project: true,
    two_factor_enabled: false,
    external: false,
    is_admin: false
  }
];

exports.dailyProps = fakeDailyProps;
exports.ccdata = fakeCCData;
exports.users = fakeUsers;
