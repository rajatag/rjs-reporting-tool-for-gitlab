import { dailyProps, ccdata, users } from "./mockData";

import DisplayDayStats from "../components/DisplayDayStats";
import DisplayTable from "../components/DisplayTable";
import NamedStatsDisplay from "../components/NamedStatsDisplay";
import DateInputForm from "../components/DateInputForm";
import MonthlyInputForm from "../components/MonthlyInputForm";
import SettingsForm from "../components/SettingsForm";
import About from "../containers/About";
import Header from "../components/Header";
import RegionOptions from "../components/RegionOptions";

const { IDBFactory, IDBKeyRange, reset } = require("shelving-mock-indexeddb");

import React from "react";
import CCInputForm from "../components/CCInputForm";
import URLDaily from "../containers/urlhandlers/URLDaily";
import URLMonthly from "../containers/urlhandlers/URLMonthly";
import URLCC from "../containers/urlhandlers/URLCC";

var MockDate = require("mockdate");

MockDate.set("1/6/2018");

window.indexedDB = new IDBFactory();

window.IDBKeyRange = IDBKeyRange;
beforeEach(() => reset());
afterEach(() => reset());

test("daily rendering snapshot", () => {
  const wrapper = mount(<DisplayDayStats {...dailyProps} />);
  expect(wrapper).toMatchSnapshot();
});

test("daily report renders name", () => {
  const wrapper = mount(<DisplayDayStats {...dailyProps} />);
  // console.log("nameth",wrapper.find('.nameth'));
  expect(wrapper.find(".nameth")).toHaveLength(1);
});

test("monthly rendering snapshot", () => {
  const wrapper = mount(<DisplayTable {...dailyProps} />);
  expect(wrapper).toMatchSnapshot();
});

test("NamedStatsDisplay rendering snapshot", () => {
  const wrapper = shallow(
    <NamedStatsDisplay
      {...dailyProps}
      ccdata={ccdata}
      addreport={() => {}}
      unique={0}
    />
  );
  expect(wrapper).toMatchSnapshot();
});

test("About is rendered", () => {
  jest.mock("../rjs.png", () => "react");

  const wrapper = shallow(<About />);
  expect(wrapper).toMatchSnapshot();
});

test("DateInputForm rendering snapshot", () => {
  const wrapper = shallow(
    <DateInputForm users={users} history={{}} loadagain={false} />
  );
  // console.log(expect(wrapper));
  expect(wrapper).toMatchSnapshot();
});

test("MonthlyInputForm rendering snapshot", () => {
  const wrapper = shallow(
    <MonthlyInputForm
      users={users}
      history={{ push: () => {} }}
      loadagain={false}
    />
  );
  expect(wrapper).toMatchSnapshot();
});

test("CCInputForm rendering snapshot", () => {
  const wrapper = shallow(
    <CCInputForm users={users} history={{ push: () => {} }} loadagain={false} />
  );
  expect(wrapper).toMatchSnapshot();
});

test("SettingsForm rendering snapshot", () => {
  const wrapper = shallow(
    <SettingsForm
      users={users}
      history={{ push: () => {} }}
      loadagain={false}
    />
  );
  expect(wrapper).toMatchSnapshot();
});

test("Header rendering snapshot", () => {
  const wrapper = shallow(
    <Header
      history={{}}
      dispatch={() => {}}
      pkey={"_xfakekeyjndsjyTnlol"}
      users={users}
      base={"example.com"}
      showMenu={true}
      sideOpts={{}}
      legend={<span />}
      goback={() => {}}
      className="screenheader"
      text={`Fake Title`}
    />
  );
  expect(wrapper).toMatchSnapshot();
});

test("RegionOptions rendering correctly", () => {
  const wrapper = shallow(<RegionOptions />);
  expect(wrapper).toMatchSnapshot();

  expect(wrapper.find("option")).toHaveLength(18);
});

test("URL Daily rendering", () => {
  const request = window.indexedDB.open("gitlab-reporter-db", 1);

  request.addEventListener("upgradeneeded", () => {
    console.log("started");
    const store = request.result.createObjectStore("gitlab-reporter-store", {
      keyPath: "#",
      autoIncrement: true
    });

    const index = store.createIndex("Key", "Key");
    const index2 = store.createIndex("Value", "Value");
  });

  request.addEventListener("success", () => {
    const putTransaction = request.result.transaction(
      ["gitlab-reporter-store"],
      "readwrite"
    );
    const putStore = putTransaction.objectStore("gitlab-reporter-store");
    putStore.put({
      Key: "gitlab-base",
      Value: "https://gitlab.example.com/api/v4"
    });
    putStore.put({ Key: "gitlab-pkey", Value: "_xfakekeyjndsjyTnlol" });
  });
  const wrapper = shallow(
    <URLDaily
      error={false}
      users={users}
      pnames={dailyProps.pnames}
      match={{
        params: {
          month: 6,
          year: 2018,
          date: 1
        }
      }}
      location={{ search: "?issuelist=true" }}
    />
  );
  expect(wrapper).toMatchSnapshot();
});

test("URL Monthly rendering", () => {
  const request = window.indexedDB.open("gitlab-reporter-db", 1);

  request.addEventListener("upgradeneeded", () => {
    console.log("started");
    const store = request.result.createObjectStore("gitlab-reporter-store", {
      keyPath: "#",
      autoIncrement: true
    });

    const index = store.createIndex("Key", "Key");
    const index2 = store.createIndex("Value", "Value");
  });

  request.addEventListener("success", () => {
    const putTransaction = request.result.transaction(
      ["gitlab-reporter-store"],
      "readwrite"
    );
    const putStore = putTransaction.objectStore("gitlab-reporter-store");
    putStore.put({
      Key: "gitlab-base",
      Value: "https://gitlab.example.com/api/v4"
    });
    putStore.put({ Key: "gitlab-pkey", Value: "_xfakekeyjndsjyTnlol" });
  });
  const wrapper = shallow(
    <URLMonthly
      error={false}
      users={users}
      pnames={dailyProps.pnames}
      match={{
        params: {
          month: 6,
          year: 2018,
          date: 1
        }
      }}
      location={{ search: "?issuelist=true" }}
    />
  );
  expect(wrapper).toMatchSnapshot();
});

test("URL CC rendering", () => {
  const request = window.indexedDB.open("gitlab-reporter-db", 1);

  request.addEventListener("upgradeneeded", () => {
    console.log("started");
    const store = request.result.createObjectStore("gitlab-reporter-store", {
      keyPath: "#",
      autoIncrement: true
    });

    const index = store.createIndex("Key", "Key");
    const index2 = store.createIndex("Value", "Value");
  });

  request.addEventListener("success", () => {
    const putTransaction = request.result.transaction(
      ["gitlab-reporter-store"],
      "readwrite"
    );
    const putStore = putTransaction.objectStore("gitlab-reporter-store");
    putStore.put({
      Key: "gitlab-base",
      Value: "https://gitlab.example.com/api/v4"
    });
    putStore.put({ Key: "gitlab-pkey", Value: "_xfakekeyjndsjyTnlol" });
  });
  const wrapper = shallow(
    <URLCC
      error={false}
      users={users}
      pnames={dailyProps.pnames}
      match={{
        params: {
          month: 6,
          year: 2018,
          date: 1
        }
      }}
    />
  );
  expect(wrapper).toMatchSnapshot();
});
