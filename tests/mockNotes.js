export default [
    {
        id : 1000,
         type : null,
         body : "unassigned @jdoe" ,
         attachment : null,
         author: {
            id: 1,
            name: "John Doe",
            username: "jdoe",
            state: "active",
            avatar_url: "",
            web_url: ""
        },
        created_at: "2018-06-19T05:49:53.854Z",
        updated_at: "2018-06-19T05:49:53.854Z",
        system: true,
        noteable_id: 112,
        noteable_type: "Issue",
        resolvable: false,
        noteable_iid: 1
    },
    {
        id: 1001,
        type: null,
        body: "added 1d 1h of time spent at 2018-05-30",
        attachment: null,
        author: {
            id: 1,
            name: "John Doe",
            username: "jdoe",
            state: "active",
            avatar_url: "",
            web_url: ""
        },
        created_at: "2018-05-30T07:46:08.817Z",
        updated_at: "2018-05-30T07:46:08.817Z",
        system: true,
        noteable_id: 113,
        noteable_type: "Issue",
        resolvable: false,
        noteable_iid: 1
    },
    {
        id: 1099,
        type: null,
        body: "I think its okay",
        attachment: null,
        author: {
            id: 1,
            name: "John Doe",
            username: "jdoe",
            state: "active",
            avatar_url: "",
            web_url: ""
        },
        created_at: "2018-05-30T07:46:08.817Z",
        updated_at: "2018-05-30T07:46:08.817Z",
        system: true,
        noteable_id: 113,
        noteable_type: "Issue",
        resolvable: false,
        noteable_iid: 1
    },
    {
        id: 1003,
        type: null,
        body: "added 3h of time spent at 2018-05-30",
        attachment: null,
        author: {
            id: 4,
            name: "Jason Mace",
            username: "jmace",
            state: "active",
            avatar_url: "",
            web_url: ""
        },
        created_at: "2018-05-30T08:46:08.817Z",
        updated_at: "2018-05-30T08:46:08.817Z",
        system: true,
        noteable_id: 113,
        noteable_type: "Issue",
        resolvable: false,
        noteable_iid: 1
    },
    {
        id: 1005,
        type: null,
        body: "added 3h of time spent at 2018-06-30",
        attachment: null,
        author: {
            id: 4,
            name: "Jason Mace",
            username: "jmace",
            state: "active",
            avatar_url: "",
            web_url: ""
        },
        created_at: "2018-06-30T08:46:08.817Z",
        updated_at: "2018-06-30T08:46:08.817Z",
        system: true,
        noteable_id: 113,
        noteable_type: "Issue",
        resolvable: false,
        noteable_iid: 1
    },
    {
        id: 1100,
        type: null,
        body: "added 3h of time spent at 2018-07-30",
        attachment: null,
        author: {
            id: 9,
            name: "Bruce Wayne",
            username: "bwayne",
            state: "active",
            avatar_url: "",
            web_url: ""
        },
        created_at: "2018-07-30T08:46:08.817Z",
        updated_at: "2018-07-30T08:46:08.817Z",
        system: true,
        noteable_id: 113,
        noteable_type: "Issue",
        resolvable: false,
        noteable_iid: 1
    },
    {
        id: 1101,
        type: null,
        body: "subtracted 1h of time spent at 2018-07-30",
        attachment: null,
        author: {
            id: 9,
            name: "Bruce Wayne",
            username: "bwayne",
            state: "active",
            avatar_url: "",
            web_url: ""
        },
        created_at: "2018-06-30T07:46:08.817Z",
        updated_at: "2018-06-30T07:46:08.817Z",
        system: true,
        noteable_id: 113,
        noteable_type: "Issue",
        resolvable: false,
        noteable_iid: 1
    },
    {
        id: 1102,
        type: null,
        body: "added 3h of time spent at 2018-07-30",
        attachment: null,
        author: {
            id: 9,
            name: "Bruce Wayne",
            username: "bwayne",
            state: "active",
            avatar_url: "",
            web_url: ""
        },
        created_at: "2018-06-30T06:46:08.817Z",
        updated_at: "2018-06-30T06:46:08.817Z",
        system: true,
        noteable_id: 113,
        noteable_type: "Issue",
        resolvable: false,
        noteable_iid: 1
    }
    


];