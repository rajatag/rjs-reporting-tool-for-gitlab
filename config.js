export default {
    KEY:'', // PRIVATE KEY with api access
    BASE:'',
    tableName:'Gitlab-Reporting-Project-CostCentre',
    colors:{
        wdoe:'#F0F4C3',// within due over estimate - yellow
        wdwe:'#fff',// within due within estimate - white
        odoe:'#f44',// over due over estimate - red
        odwe:'#EF9A9A',// over due within estimate - light red
        duemet:'#A5D6A7',//due met - green
        duemiss:'#E57373',//due miss - red
        wdne:'#fff',//within due no estimate - white
        ndwe:'#fff',//no due within estimate - white
        odne:'#fff',//over due no estimate - red
        ndoe:'#f44',//no due over estimate - white
        nodue:'#FFCC80'
    }
};