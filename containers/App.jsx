import React, { Component } from "react";

import DateInputForm from "../components/DateInputForm";
import MonthInputForm from "../components/CCInputForm";
import SettingsForm from "../components/SettingsForm";

import MiniSettingsForm from "../components/MiniSettingsForm";

import { Store, get, set } from "idb-keyval";

import "../styles/global.css";
import Header from "../components/Header";
import MultiInputForm from "../components/MonthlyInputForm";

// this handles everything in the "Reports" section
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      keyset: false,
      key: "",
      multidata: {},
      showMulti: false,
      daySelect: true,
      showDate: true,
      onlydate: -1,
      date: "",
      showAllStats: false,
      monthSelect: false,
      settings: false,
      base: "",
      baseval: "",
      endpoint: "https://dynamodb.us-east-2.amazonaws.com",
      secret: "",
      access: "",
      region: "us-east-2",
      imageURL: "",
      users: this.props.users,
      error: false,
      errorMsg: "",
      loaded: false,
      filter: ""
    };

    this.customStore = new Store("gitlab-reporter-db", "gitlab-reporter-store");
  }

  componentDidMount() {
    const search = this.props.location.search;
    const params = new URLSearchParams(search);

    switch (params.get("tab")) {
      case "monthly":
        this.doMonthly();
        break;
      case "daily":
        this.doDaily();
        break;
      case "cc":
        this.doCC();
        break;
      case "settings":
        this.doSettings();
        break;
    }

    Promise.all([
      get("gitlab-pkey", this.customStore),
      get("gitlab-pkey", this.customStore),
      get("gitlab-dynamo-access", this.customStore),
      get("gitlab-dynamo-secret", this.customStore),
      get("gitlab-dynamo-endpoint", this.customStore),
      get("gitlab-dynamo-region", this.customStore)
    ]).then(data => {
      let val = data[0];
      let base = data[1];
      let access = data[2];
      let secret = data[3];
      let endpoint = data[4];
      let region = data[5];

      if (val) {
        this.setState({ key: val, loaded: true, keyset: true, base: base });
      } else {
        this.setState({ loaded: true });
      }

      AWS.config.update({
        region: region,
        endpoint: endpoint,
        accessKeyId: access,
        secretAccessKey: secret
      });
    });
  }
  handleBack() {
    this.setState({
      showMulti: false
    });
  }

  doMonthly() {
    history.replaceState("", "Gitlab Reporter", "/?tab=monthly");
    this.setState({
      daySelect: false,
      showDate: false,
      monthSelect: false,
      showAllStats: false,
      settings: false,
      showMulti: false
    });
  }
  doDaily() {
    history.replaceState("", "Gitlab Reporter", "/?tab=daily");
    this.setState({
      daySelect: true,
      showDate: true,
      monthSelect: false,
      showAllStats: false,
      settings: false,
      showMulti: false
    });
  }
  doCC() {
    history.replaceState("", "Gitlab Reporter", "/?tab=cc");
    this.setState({
      daySelect: false,
      showDate: false,
      monthSelect: true,
      showAllStats: true,
      settings: false,
      showMulti: false
    });
  }
  doSettings() {
    history.replaceState("", "Gitlab Reporter", "/?tab=settings");
    this.setState({
      daySelect: false,
      showDate: false,
      monthSelect: false,
      showAllStats: false,
      settings: true,
      showMulti: false
    });
  }
  hanldeFilterUsersChange(evt) {
    this.setState({ filter: evt.target.value });
  }
  render() {
    if (this.state.error || this.props.error) {
      return (
        <div>
          <div
            style={{
              padding: "2em"
            }}
            className="formerror"
          >
            ERROR<br />
            {this.state.errorMsg}
          </div>
          <br />
          <MiniSettingsForm base={this.state.base} pkey={this.state.key} />
        </div>
      );
    }
    if (!this.state.loaded) {
      return (
        <div className="inputform prgrs">
          <div className="progress">
            <div
              className="progress-bar progress-bar-striped progress-bar-animated"
              role="progressbar"
              aria-valuenow="100"
              aria-valuemin="0"
              aria-valuemax="100"
              style={{ width: "100%" }}
            />
          </div>
        </div>
      );
    }

    if (this.state.settings) {
      return (
        <div>
          <Header
            history={this.props.history}
            showMenu={true}
            legend={<span />}
            className="screenheader"
            text={"Gitlab Reporter"}
          />
          <div className="inputform tablayout">
            <ul className="nav nav-tabs">
              <li className="nav-item">
                <button
                  className="nav-link"
                  onClick={evt => {
                    this.doDaily();
                  }}
                >
                  Daily
                </button>
              </li>
              <li className="nav-item">
                <button
                  className="nav-link"
                  onClick={evt => {
                    this.doMonthly();
                  }}
                >
                  Monthly
                </button>
              </li>

              <li
                className="nav-item"
                onClick={evt => {
                  this.doCC();
                }}
              >
                <button className="nav-link">Cost Centre</button>
              </li>
              <li>
                <button className="nav-link active">Settings</button>
              </li>
            </ul>
            <SettingsForm
              history={this.props.history}
              users={this.state.users}
              base={this.state.base}
              pkey={this.state.key}
            />
          </div>
        </div>
      );
    }
    if (this.state.daySelect) {
      return (
        <div>
          <Header
            history={this.props.history}
            showMenu={true}
            legend={<span />}
            className="screenheader"
            text={"Gitlab Reporter"}
          />
          <div className="inputform tablayout">
            <ul className="nav nav-tabs">
              <li className="nav-item">
                <button
                  className="nav-link active"
                  onClick={evt => {
                    this.doDaily();
                  }}
                >
                  Daily
                </button>
              </li>
              <li className="nav-item">
                <button
                  className="nav-link"
                  onClick={evt => {
                    this.doMonthly();
                  }}
                >
                  Monthly
                </button>
              </li>

              <li
                className="nav-item"
                onClick={evt => {
                  this.doCC();
                }}
              >
                <button className="nav-link">Cost Centre</button>
              </li>
              <li
                onClick={evt => {
                  this.doSettings();
                }}
              >
                <button className="nav-link">Settings</button>
              </li>
            </ul>
            <DateInputForm
              loadagain={false}
              history={this.props.history}
              users={this.state.users}
            />
          </div>
        </div>
      );
    }
    if (this.state.monthSelect) {
      return (
        <div>
          <Header
            history={this.props.history}
            showMenu={true}
            legend={<span />}
            className="screenheader"
            text={"Gitlab Reporter"}
          />
          <div className="inputform tablayout">
            <ul className="nav nav-tabs">
              <li className="nav-item">
                <button
                  className="nav-link"
                  onClick={evt => {
                    this.doDaily();
                  }}
                >
                  Daily
                </button>
              </li>
              <li className="nav-item">
                <button
                  className="nav-link"
                  onClick={evt => {
                    this.doMonthly();
                  }}
                >
                  Monthly
                </button>
              </li>

              <li className="nav-item">
                <a className="nav-link active">Cost Centre</a>
              </li>
              <li
                onClick={evt => {
                  this.doSettings();
                }}
              >
                <button className="nav-link">Settings</button>
              </li>
            </ul>
            <MonthInputForm
              loadagain={false}
              history={this.props.history}
              users={this.state.users}
            />
          </div>
        </div>
      );
    }
    console.log("app-state", this.state.users);
    return (
      <div>
        <Header
          history={this.props.history}
          showMenu={true}
          legend={<span />}
          className="screenheader"
          text={"Gitlab Reporter"}
        />
        <div className="inputform tablayout">
          <ul className="nav nav-tabs">
            <li className="nav-item">
              <button
                className="nav-link"
                onClick={evt => {
                  this.doDaily();
                }}
              >
                Daily
              </button>
            </li>
            <li className="nav-item">
              <a className="nav-link active">Monthly</a>
            </li>

            <li
              className="nav-item"
              onClick={evt => {
                this.doCC();
              }}
            >
              <button className="nav-link">Cost Centre</button>
            </li>
            <li
              onClick={evt => {
                this.doSettings();
              }}
            >
              <button className="nav-link">Settings</button>
            </li>
          </ul>

          <MultiInputForm
            loadagain={false}
            history={this.props.history}
            users={this.state.users}
            base={this.state.base}
          />
        </div>
      </div>
    );
  }
  handleMonthlyInput(data) {
    this.setState({ multidata: data, showAllStats: true });
  }
}
