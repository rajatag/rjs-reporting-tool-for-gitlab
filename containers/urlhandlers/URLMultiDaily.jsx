import React, { Component } from "react";
import { Store, get, set } from "idb-keyval";
import MultiReport from "../MultiReport";
import MiniSettingsForm from "../../components/MiniSettingsForm";

//to generate CC report for one or more users
export default class URLMultiDaily extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loaded: false,
      error: false,
      errorMsg: "",
      users: this.props.users,
      key: "",
      base: "",
      pnames: this.props.pnames
    };
    this.customStore = new Store("gitlab-reporter-db", "gitlab-reporter-store");
    this.userids = [];
    this.names = [];
    this.allputData = [];
    this.month = parseInt(this.props.match.params.month);
    this.year = parseInt(this.props.match.params.year);
    this.date = parseInt(this.props.match.params.date);

    let userParams = decodeURI(this.props.match.params.userids).split(",");
    console.log("params", userParams);
    this.userids = userParams.map(user => {
      return parseInt(user);
    });
  }
  getUserIds() {
    let ids = [];
    for (let user of this.state.users) {
      ids.push(user.id);
    }
    return ids;
  }
  getNames() {
    let names = [];
    for (let id of this.userids) {
      for (let user of this.state.users) {
        if (id == user.id) {
          console.log("match", id, user);
          names.push(user.name);
        }
      }
    }
    return names;
  }

  componentDidMount() {
    const search = this.props.location.search;
    const params = new URLSearchParams(search);

    let isl = params.get("issuelist");
    if (isl != null) {
      if (isl == "true") {
        this.issuelist = true;
      }
    }
    console.log("issuelist", this.issuelist);
    get("gitlab-pkey", this.customStore).then(val => {
      if (val) {
        console.log("key", val);
        get("gitlab-base", this.customStore).then(base => {
          console.log("fetching");

          this.setState({ key: val, loaded: true, base: base });
        });
      } else {
        this.setState({ error: true, errorMsg: "Please Log In" });
      }
    });
  }
  handleBack() {
    this.props.history.push("/");
  }
  render() {
    if (this.state.error) {
      return (
        <div>
          <div
            style={{
              padding: "2em"
            }}
            className="formerror"
          >
            ERROR<br />
            {this.state.errorMsg}
          </div>
          <br />
          <MiniSettingsForm
            base={this.state.base}
            pkey={this.state.key}
            dispatch={this.handleMultiInput}
          />
        </div>
      );
    }
    if (!this.state.loaded) {
      return (
        <div className="inputform prgrs">
          <div className="progress">
            <div
              className="progress-bar progress-bar-striped progress-bar-animated"
              role="progressbar"
              aria-valuenow="100"
              aria-valuemin="0"
              aria-valuemax="100"
              style={{ width: "100%" }}
            />
          </div>
        </div>
      );
    }
    this.names = this.getNames();
    let multiData = {
      userids: this.userids,
      onlydate: this.date,
      month: this.month,
      year: this.year,
      names: this.names
    };
    console.log(multiData);
    return (
      <MultiReport
        issuelist={this.issuelist}
        pnames={this.state.pnames}
        ccError={""}
        history={this.props.history}
        dispatch={() => {}}
        users={this.state.users}
        sideOpts={{}}
        goback={this.handleBack.bind(this)}
        base={this.state.base}
        showAllStats={false}
        showDate={true}
        pkey={this.state.key}
        data={multiData}
      />
    );
  }
}
