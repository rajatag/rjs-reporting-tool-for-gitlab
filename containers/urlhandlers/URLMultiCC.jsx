import React, { Component } from "react";
import { Store, get, set } from "idb-keyval";
import MultiReport from "../MultiReport";
import getAllData from "../../awsutil/getAllData";
import MiniSettingsForm from "../../components/MiniSettingsForm";
import { Link } from "react-router-dom";

export default class URLMultiCC extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loaded: false,
      error: false,
      errorMsg: "",
      users: this.props.users,
      key: "",
      base: "",
      ccload: false,
      ccData: [],
      ccError: "",
      pnames: this.props.pnames
    };
    this.customStore = new Store("gitlab-reporter-db", "gitlab-reporter-store");
    this.userids = [];
    this.names = [];

    this.month = parseInt(this.props.match.params.month);
    this.year = parseInt(this.props.match.params.year);
    this.allputData = [];
    let userParams = decodeURI(this.props.match.params.userids).split(",");
    console.log("params", userParams);
    this.userids = userParams.map(user => {
      return parseInt(user);
    });
  }
  getUserIds() {
    let ids = [];
    for (let user of this.state.users) {
      ids.push(user.id);
    }
    return ids;
  }
  getNames() {
    let names = [];
    for (let id of this.userids) {
      for (let user of this.state.users) {
        if (id == user.id) {
          console.log("match", id, user);
          names.push(user.name);
        }
      }
    }
    return names;
  }

  componentDidMount() {
    Promise.all([
      get("gitlab-dynamo-access", this.customStore),
      get("gitlab-dynamo-secret", this.customStore),
      get("gitlab-dynamo-endpoint", this.customStore),
      get("gitlab-dynamo-region", this.customStore),
      get("gitlab-pkey", this.customStore),
      get("gitlab-base", this.customStore)
    ]).then(data => {
      let access = data[0];
      let secret = data[1];
      let endpoint = data[2];
      let region = data[3];
      let val = data[4];
      let base = data[5];

      if (val) {
        this.setState({ key: val, loaded: true, base: base });
      } else {
        this.setState({ error: true, errorMsg: "Please Log In" });
      }

      AWS.config.update({
        region: region,
        endpoint: endpoint,
        accessKeyId: access,
        secretAccessKey: secret
      });

      getAllData((err, data) => {
        if (err) {
          console.log("scan error", err);
          this.setState({
            ccload: true,
            ccError: (
              <Link to="/?tab=settings">
                {" "}
                <span className="ccerror">
                  <i class="fas fa-exclamation-triangle" /> Check AWS
                  Credentials
                </span>
              </Link>
            )
          });
        } else {
          console.log("scan data", data);
          let ccMap = [];
          for (let item of data.Items) {
            ccMap[item.pid] = item.ccname;
          }
          this.setState({ ccload: true, ccData: ccMap });
        }
      });
    });
  }
  handleBack() {
    this.props.history.push("/");
  }
  render() {
    if (this.props.error || this.state.error) {
      return (
        <div>
          <div
            style={{
              padding: "2em"
            }}
            className="formerror"
          >
            ERROR<br />
            {this.state.errorMsg}
          </div>
          <br />
          <MiniSettingsForm
            base={this.state.base}
            pkey={this.state.key}
            dispatch={this.handleMultiInput}
          />
        </div>
      );
    }
    if (!this.state.loaded || !this.state.ccload) {
      return (
        <div className="inputform prgrs">
          <div style={{ padding: "1em" }} className="ctd">
            Loading DynamoDB data
          </div>
          <div className="progress">
            <div
              className="progress-bar progress-bar-striped progress-bar-animated"
              role="progressbar"
              aria-valuenow="100"
              aria-valuemin="0"
              aria-valuemax="100"
              style={{ width: "100%" }}
            />
          </div>
        </div>
      );
    }
    this.names = this.getNames();
    let multiData = {
      userids: this.userids,
      month: this.month,
      year: this.year,
      names: this.names
    };
    console.log(multiData);
    return (
      <MultiReport
        pnames={this.state.pnames}
        ccError={this.state.ccError}
        ccdata={this.state.ccData}
        history={this.props.history}
        dispatch={() => {}}
        users={this.state.users}
        sideOpts={{}}
        goback={this.handleBack.bind(this)}
        base={this.state.base}
        showAllStats={true}
        showDate={false}
        pkey={this.state.key}
        data={multiData}
      />
    );
  }
}
