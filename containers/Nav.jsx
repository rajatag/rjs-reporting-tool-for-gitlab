import React, { Component } from "react";
import App from "../containers/App";
import DBManager from "../containers/DBManager";
import URLDaily from "../containers/urlhandlers/URLDaily";
import URLCC from "../containers/urlhandlers/URLCC";
import URLMonthly from "../containers/urlhandlers/URLMonthly";

import Error404 from "../containers/Erorr404";
import About from "../containers/About";

import { BrowserRouter as Router } from "react-router-dom";
import { Route, Switch } from "react-router";
import { Link } from "react-router-dom";

import URLMultiDaily from "../containers/urlhandlers/URLMultiDaily";
import URLMultiCC from "../containers/urlhandlers/URLMultiCC";

import { get, Store } from "idb-keyval";
import MiniSettings from "../components/MiniSettingsForm";
import Header from "../components/Header";

import Modal from "react-modal";
import axios from "axios";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    padding: "4em"
  }
};

//Top Level nav component

export default class Nav extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loaded: false,
      pnames: [],
      error: false,
      errorMsg: "Error reaching Gitlab. You might have got the Base URL wrong.",
      users: [],
      key: "",
      val: "",
      is400: false,
      firstLogin: false,
      errorModal: true,
      porgressShow: "Loading Users..."
    };
    this.allputData = [];
    this.customStore = new Store("gitlab-reporter-db", "gitlab-reporter-store");

    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.projectLoaded = 0;
  }

  openModal() {
    this.setState({ errorModal: true });
  }

  closeModal() {
    this.setState({ errorModal: false });
  }

  getAllProjects() {
    let startPage = 1;
    this.setState({ porgressShow: "Loading Projects - 0%" });
    return axios
      .get(this.state.base + "/projects?per_page=100", {
        headers: {
          "PRIVATE-TOKEN": this.state.key
        },
        onDownloadProgress: data => {
          console.log("progress", data);
        }
      })
      .then(response => {
        if (response.status == 401) {
          this.setState({
            error: true,
            is400: true,
            errorMsg: "Check your Gitlab Personal Access Token"
          });
          return;
        }
        if (response.status != 200) {
          this.setState({ error: true });
          return;
        }

        console.log("axios response", response.headers);
        console.log("axios response", response);

        this.projectNextPage = parseInt(response.headers["x-next-page"]);
        this.projectTotalPages = parseInt(response.headers["x-total-pages"]);

        this.projectLoaded += 100;
        let totalProjects = parseInt(response.headers["x-total"]);

        let perc = Math.round((this.projectLoaded / totalProjects) * 100);

        if (this.projectLoaded > totalProjects) {
          this.setState({ porgressShow: "Loading Projects - 100%" });
        } else {
          this.setState({ porgressShow: `Loading Projects - ${perc}%` });
        }

        for (let project of response.data) {
          this.allputData[project.id] = {};
          this.allputData[project.id].name = project.name;
          this.allputData[project.id].url = project.web_url;
          this.allputData[project.id].pid = project.pid;
          this.allputData[project.id].name_with_namespace =
            project.name_with_namespace;
        }

        if (this.projectTotalPages > startPage) {
          return this.getAllProjectPages();
        }
      });
    // .then(data => {

    // });
  }
  getAllProjectPages() {
    let allURL = [];
    for (let i = this.projectNextPage; i <= this.projectTotalPages; i++) {
      allURL.push(`${this.state.base}/projects?per_page=100&page=${i}`);
    }
    console.log("all", allURL);
    return Promise.all(
      allURL.map(url =>
        axios.get(url, {
          headers: {
            "PRIVATE-TOKEN": this.state.key
          },
          onDownloadProgress: data => {
            console.log("progress", data);
          }
        })
      )
    ).then(resp => {
      console.log("response data", resp);

      for (let rs of resp) {
        let jsn = rs.data;

        this.projectLoaded += 100;
        let totalProjects = parseInt(rs.headers["x-total"]);

        let perc = Math.round((this.projectLoaded / totalProjects) * 100);

        if (this.projectLoaded > totalProjects) {
          this.setState({ porgressShow: "Loading Projects - 100%" });
        } else {
          this.setState({ porgressShow: `Loading Projects - ${perc}%` });
        }

        for (let j of jsn) {
          this.allputData[j.id] = {};
          this.allputData[j.id].name = j.name;
          this.allputData[j.id].url = j.web_url;
          this.allputData[j.id].pid = j.pid;
          this.allputData[j.id].name_with_namespace = j.name_with_namespace;
        }
      }
    });
    // .then(json => {
    //     console.log("prj-jsn", json);

    // });
  }

  getAllUsers() {
    this.setState({ loaded: false });
    return axios
      .get(this.state.base + "/users?per_page=100&order_by=name&sort=asc", {
        headers: {
          "PRIVATE-TOKEN": this.state.key
        },
        onDownloadProgress: data => {
          console.log("progress", data);
        }
      })
      .then(res => {
        if (res.status == 401) {
          this.setState({
            error: true,
            is400: true,
            errorMsg: "Check Gitlab Personal Acess Token",
            loaded: true
          });
          return;
        }
        if (res.status != 200) {
          this.setState({ error: true, loaded: true });
          return;
        }
        let data = res.data;
        get("gitlab-filter-users", this.customStore).then(filter => {
          let filteredNames = [];
          if (!filter) filter = " ";
          filteredNames = filter.split(",");

          filteredNames = filteredNames.map(item => {
            return item.trim();
          });

          console.log("filtered names", filteredNames);
          let newUsers = [];
          if (data) {
            console.log(data);
            newUsers = data.filter(user => {
              if (filteredNames.indexOf(user.username) == -1) {
                return true;
              } else return false;
            });
          } else {
            newUsers = [];
          }

          this.getAllProjects().then(() => {
            this.setState({
              users: newUsers,
              loaded: true,
              pnames: this.allputData
            });
          });
        });
      })
      .catch(err => {
        console.log("catch err", err);
        this.setState({ error: true, loaded: true });
        return;
      });
  }

  componentDidMount() {
    //All the data about projects and users are loaded here and passed down the components as props
    get("gitlab-pkey", this.customStore).then(val => {
      if (val) {
        console.log("key", val);
        get("gitlab-base", this.customStore).then(base => {
          console.log("fetching");

          this.setState({ key: val, base: base }, this.getAllUsers);
        });
      } else {
        this.setState({
          error: true,
          errorMsg: "Please Log In",
          firstLogin: true,
          loaded: true
        });
      }
    });
  }

  render() {
    if (this.state.error) {
      return (
        <div>
          <Router>
            <Switch>
              <Route
                exact
                path="/about"
                render={props => (
                  <About
                    {...props}
                    users={this.state.users}
                    loaded={this.state.loaded}
                    pnames={this.state.pnames}
                    error={this.state.error}
                    errorMsg={this.state.errorMsg}
                  />
                )}
              />
              <Route
                render={props => {
                  console.log("error-state", this.state);

                  if (this.state.firstLogin) {
                    return (
                      <div>
                        <Header
                          history={this.props.history}
                          showMenu={true}
                          legend={<span />}
                          className="screenheader"
                          text={"Gitlab Reporter"}
                        />

                        <MiniSettings
                          base={this.state.base}
                          pkey={this.state.key}
                          dispatch={this.handleMultiInput}
                        />

                        <div className="ctd italics">
                          First time here ? Check out{" "}
                          <Link to="/about">
                            <span style={{ color: "#007bff" }}>the intro</span>
                          </Link>
                        </div>
                      </div>
                    );
                  }

                  return (
                    <div>
                      <Header
                        history={this.props.history}
                        showMenu={true}
                        legend={<span />}
                        className="screenheader"
                        text={"Gitlab Reporter"}
                      />

                      <Modal
                        isOpen={this.state.errorModal}
                        onRequestClose={this.closeModal}
                        style={customStyles}
                        contentLabel="Example Modal"
                      >
                        <h2 className="ctd" style={{ color: "#f44" }}>
                          Error
                        </h2>

                        <h4 className="ctd">{this.state.errorMsg}</h4>

                        <button
                          onClick={this.closeModal}
                          style={{ float: "right" }}
                          className="btn btn-danger"
                        >
                          Okay, I understand
                        </button>
                      </Modal>

                      <MiniSettings
                        base={this.state.base}
                        pkey={this.state.key}
                        dispatch={this.handleMultiInput}
                      />
                    </div>
                  );
                }}
              />
            </Switch>
          </Router>
        </div>
      );
    }
    if (!this.state.loaded) {
      return (
        <div className="inputform prgrs">
          <div style={{ padding: "1em" }} className="ctd">
            {this.state.porgressShow}
          </div>
          <div className="progress">
            <div
              className="progress-bar progress-bar-striped progress-bar-animated"
              role="progressbar"
              aria-valuenow="100"
              aria-valuemin="0"
              aria-valuemax="100"
              style={{ width: "100%" }}
            />
          </div>
        </div>
      );
    }
    return (
      <Router>
        <Switch>
          {/*  */}
          <Route
            exact
            path="/"
            render={props => (
              <App
                {...props}
                users={this.state.users}
                loaded={this.state.loaded}
                pnames={this.state.pnames}
                error={this.state.error}
                errorMsg={this.state.errorMsg}
              />
            )}
          />
          <Route
            exact
            path="/report"
            render={props => (
              <App
                {...props}
                users={this.state.users}
                loaded={this.state.loaded}
                pnames={this.state.pnames}
                error={this.state.error}
                errorMsg={this.state.errorMsg}
              />
            )}
          />
          <Route
            exact
            path="/dbmanager"
            render={props => (
              <DBManager
                {...props}
                users={this.state.users}
                loaded={this.state.loaded}
                pnames={this.state.pnames}
                error={this.state.error}
                errorMsg={this.state.errorMsg}
              />
            )}
          />

          <Route
            exact
            path="/about"
            render={props => (
              <About
                {...props}
                users={this.state.users}
                loaded={this.state.loaded}
                pnames={this.state.pnames}
                error={this.state.error}
                errorMsg={this.state.errorMsg}
              />
            )}
          />

          <Route
            exact
            path="/daily/:year/:month/:date"
            render={props => (
              <URLDaily
                {...props}
                users={this.state.users}
                loaded={this.state.loaded}
                pnames={this.state.pnames}
                error={this.state.error}
                errorMsg={this.state.errorMsg}
              />
            )}
          />
          <Route
            exact
            path="/daily/:year/:month/:date/:userids"
            render={props => (
              <URLMultiDaily
                {...props}
                users={this.state.users}
                loaded={this.state.loaded}
                pnames={this.state.pnames}
                error={this.state.error}
                errorMsg={this.state.errorMsg}
              />
            )}
          />

          <Route
            exact
            path="/cc/:year/:month"
            render={props => (
              <URLCC
                {...props}
                loaded={this.state.loaded}
                users={this.state.users}
                pnames={this.state.pnames}
                error={this.state.error}
                errorMsg={this.state.errorMsg}
              />
            )}
          />
          <Route
            exact
            path="/cc/:year/:month/:userids"
            render={props => (
              <URLMultiCC
                {...props}
                loaded={this.state.loaded}
                users={this.state.users}
                pnames={this.state.pnames}
                error={this.state.error}
                errorMsg={this.state.errorMsg}
              />
            )}
          />

          <Route
            exact
            path="/monthly/:year/:month/:userids"
            render={props => (
              <URLMonthly
                {...props}
                loaded={this.state.loaded}
                users={this.state.users}
                pnames={this.state.pnames}
                error={this.state.error}
                errorMsg={this.state.errorMsg}
              />
            )}
          />

          <Route component={Error404} />
        </Switch>
      </Router>
    );
  }
}
