import React, { Component } from "react";
import Report from "./Report";
import Header from "../components/Header";
import LegendItem from "../components/LegendItem";
import config from "../config";
import { CSVLink } from "react-csv";
import dailyPrint from "../printutil/dailyPrint";

//holder for multiple report instances -  reports for multiple users one by one
export default class MultiReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fullReport: [],
      fullLoad: false,
      reportElements: [
        <Report
          pnames={this.props.pnames}
          issuelist={this.props.issuelist}
          total={this.props.data.userids.length}
          done={this.handleReportDone.bind(this)}
          ccdata={this.props.ccdata}
          addreport={this.addReport.bind(this)}
          unique={0}
          base={this.props.base}
          showAllStats={this.props.showAllStats}
          showDate={this.props.showDate}
          onlydate={this.props.data.onlydate}
          pkey={this.props.pkey}
          key={0}
          userid={this.props.data.userids[0]}
          name={this.props.data.names[0]}
          mode="multi"
          month={this.props.data.month}
          year={this.props.data.year}
        />
      ]
    };
    this.fR = [["Name", "Salary", "Type", "Cost Center", "Allocation %"]];
    this.numGot = 0;
    this.totalIDs = this.props.data.userids.length;
    this.reportElements = [];
  }
  handleReportDone(id) {
    if (id < this.props.data.userids.length - 1) {
      let repEl = [...this.state.reportElements];
      repEl.push(
        <Report
          pnames={this.props.pnames}
          issuelist={this.props.issuelist}
          total={this.props.data.userids.length}
          done={this.handleReportDone.bind(this)}
          ccdata={this.props.ccdata}
          addreport={this.addReport.bind(this)}
          unique={id + 1}
          base={this.props.base}
          showAllStats={this.props.showAllStats}
          showDate={this.props.showDate}
          onlydate={this.props.data.onlydate}
          pkey={this.props.pkey}
          key={id + 1}
          userid={this.props.data.userids[id + 1]}
          name={this.props.data.names[id + 1]}
          mode="multi"
          month={this.props.data.month}
          year={this.props.data.year}
        />
      );
      this.setState({ reportElements: repEl });
    }
  }

  addReport(op, data, sentOnce) {
    if (op == "del" && !sentOnce) {
      this.totalIDs--;
      return;
    }
    if (!sentOnce) {
      this.numGot++;
    }
    this.fR = this.fR.concat(data);

    console.log("report-add list", this.fR);
    console.log("report-add data", data);
    console.log("numgotuser", data, this.totalIDs, this.numGot);

    if (this.totalIDs == this.numGot) {
      console.log();
      this.setState({ fullReport: this.fR, fullLoad: true });
    }
  }
  render() {
    let textmonths = [
      "",
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Sep",
      "Oct",
      "Nov",
      "Dec"
    ];

    let styleElem = (
      <style type="text/css">
        {`@media print{@page {size: landscape}
          th{
            padding:0.3em !important;
            padding-top:0em !important;
          }
          .projectname{
            padding:0.3em !important;
            background: #e9e9e9;
          }
        }  .report{
              page-break-after: always;
          }
          `}
      </style>
    );
    if (this.props.showDate || this.props.showAllStats) {
      styleElem = (
        <style type="text/css">
          {`@media print
        {@page {size: portrait;}
        }`}
        </style>
      );
    }
    let headerElement;
    let fixHeader;
    let legendElement = (
      <div className="legend">
        <LegendItem title="Due date met" color={config.colors.duemet} />
        <br />
        <LegendItem title="Due date missed" color={config.colors.duemiss} />
        <br />
        <LegendItem title="No due date set" color={config.colors.nodue} />
      </div>
    );
    if (this.props.showDate) {
      legendElement = (
        <div className="legend">
          <LegendItem title="No due date set" color={config.colors.nodue} />
          <LegendItem title="No records entered" color={"#ffcccc"} />
          <button
            onClick={() => {
              dailyPrint(this.props);
            }}
            className="btn btn-success"
          >
            <i style={{ color: "#fff" }} class="fas fa-download" /> Download PDF
          </button>
        </div>
      );
    }
    let csvEl = (
      <span>
        {this.props.ccError}
        <button className="btn btn-success dcsv">
          <div className="lds-ring">
            <div />
            <div />
            <div />
            <div />
          </div>
        </button>
      </span>
    );

    if (this.props.showAllStats && this.state.fullLoad) {
      legendElement = <span />;
      csvEl = (
        <span>
          {this.props.ccError}
          <CSVLink filename={`COST-REPORT.csv`} data={this.state.fullReport}>
            <button className="btn btn-success">
              <i style={{ color: "#fff" }} class="fas fa-download" /> Download
              CSV
            </button>
          </CSVLink>
        </span>
      );
    }
    if (this.props.showDate) {
      if (this.props.issuelist) {
        fixHeader = (
          <Header
            history={this.props.history}
            dispatch={this.props.dispatch}
            pkey={this.props.pkey}
            users={this.props.users}
            base={this.props.base}
            showMenu={true}
            sideOpts={this.props.sideOpts}
            legend={legendElement}
            goback={this.props.goback}
            className="screenheader"
            text={`Issue List for ${this.props.data.onlydate}-${
              textmonths[this.props.data.month]
            }-${this.props.data.year}`}
          />
        );
      } else {
        fixHeader = (
          <Header
            history={this.props.history}
            dispatch={this.props.dispatch}
            pkey={this.props.pkey}
            users={this.props.users}
            base={this.props.base}
            showMenu={true}
            sideOpts={this.props.sideOpts}
            legend={legendElement}
            goback={this.props.goback}
            className="screenheader"
            text={`Report for ${this.props.data.onlydate}-${
              textmonths[this.props.data.month]
            }-${this.props.data.year}`}
          />
        );
      }
    } else
      fixHeader = (
        <Header
          history={this.props.history}
          dispatch={this.props.dispatch}
          pkey={this.props.pkey}
          users={this.props.users}
          base={this.props.base}
          showMenu={true}
          sideOpts={this.props.sideOpts}
          legend={legendElement}
          goback={this.props.goback}
          className="screenheader"
          text={`Report for ${textmonths[this.props.data.month]} ${
            this.props.data.year
          }`}
        />
      );

    if (this.props.showDate) {
      headerElement = (
        <thead>
          <th className="reporttd reportth ctd">
            <span className="printtext">
              {`Report for ${this.props.data.onlydate}-${
                textmonths[this.props.data.month]
              }-${this.props.data.year}`}
            </span>
            <br />
            <span className="printlonly">{legendElement}</span>
          </th>
        </thead>
      );
    } else
      headerElement = (
        <thead>
          <th className="reporttd reportth ctd">
            <span className="printtext">{`Report for ${
              textmonths[this.props.data.month]
            } ${this.props.data.year}`}</span>
            <br /> <span className="printlonly">{legendElement}</span>
          </th>
        </thead>
      );

    // console.log("csv el",csvEl);
    if (this.props.showAllStats) {
      fixHeader = (
        <Header
          dispatch={this.props.dispatch}
          history={this.props.history}
          pkey={this.props.pkey}
          users={this.props.users}
          base={this.props.base}
          showMenu={true}
          sideOpts={this.props.sideOpts}
          legend={csvEl}
          goback={this.props.goback}
          className="screenheader"
          text={`Cost Center Report for ${textmonths[this.props.data.month]} ${
            this.props.data.year
          }`}
        />
      );

      headerElement = (
        <thead>
          <td className="reporttd reportth ctd">
            <span className="printtext">{`Cost Center Report for ${
              textmonths[this.props.data.month]
            } ${this.props.data.year}`}</span>
          </td>
        </thead>
      );
    }

    if (this.props.showDate) {
      return (
        <section>
          {fixHeader}
          <table className="strechtable">
            {headerElement}
            {styleElem}
            <tr>
              <td className="reporttd">
                <div id="test" style={{ height: "10pt" }} />
                {this.state.reportElements}
              </td>
            </tr>
          </table>
        </section>
      );
    }
    return (
      <section>
        {fixHeader}
        <table className="strechtable">
          {headerElement}
          {styleElem}
          <tr>
            <td className="reporttd monthlytd">
              <div id="test" style={{ height: "10pt" }} />
              {this.state.reportElements}
            </td>
          </tr>
        </table>
      </section>
    );
  }
}
