import React from "react";
import { CSVLink } from "react-csv";

export default class StatsDisplay extends React.Component {
  constructor(props) {
    super(props);
    console.log("purls", this.props);

    this.dataArray = [];
    console.log("funcreport3", this.props.addreport);

    this.sentArray = [];
    for (let pid in this.props.data) {
      this.sentArray[pid] = false;
    }
    this.sentOnce = false;

    console.log("pnames in stats display", this.props.pnames);
  }
  render() {
    this.projectStats = [];
    this.csvData = [];
    let printData = [];
    let totalProjectTime = 0;
    for (let pid in this.props.data) {
      let project = this.props.data[pid];
      if (project.issues.length == 0) {
        continue;
      }
      let totalHours = 0;
      let totalBugs = 0;
      let totalFeatures = 0;
      console.log("pid>>>", pid);

      for (let issue of project.issues) {
        console.log(issue);
        for (let day = 1; day <= 31; day++) {
          totalHours += issue.dateData[day];
          if (issue.type === "bug") {
            totalBugs += issue.dateData[day];
          }
          if (issue.type === "feature") {
            totalFeatures += issue.dateData[day];
          }
        }
      }

      totalProjectTime += totalHours;

      console.log(totalBugs, totalFeatures, totalHours);
      let bugPerc = Math.ceil((totalBugs / totalHours) * 100);
      let featPerc = Math.floor((totalFeatures / totalHours) * 100);

      if (Number.isNaN(bugPerc)) {
        bugPerc = 0;
      }
      if (Number.isNaN(featPerc)) {
        featPerc = 0;
      }

      if (featPerc == 0 && bugPerc == 0) {
        continue;
      }

      this.projectStats[pid] = {
        bugs: bugPerc,
        features: featPerc
      };
    }
    let fullData = [];
    let totalPerc = 0.0;
    for (let pid in this.props.data) {
      let totalBugs = 0;
      let totalFeatures = 0;

      console.log("pid>>>", pid);
      let project = this.props.data[pid];
      if (project.issues.length == 0) {
        continue;
      }

      for (let issue of project.issues) {
        console.log(issue);
        for (let day = 1; day <= 31; day++) {
          if (issue.type === "bug") {
            totalBugs += issue.dateData[day];
          }
          if (issue.type === "feature") {
            totalFeatures += issue.dateData[day];
          }
        }
      }
      printData[pid] = {
        bugs: 0,
        features: 0
      };
      fullData[pid] = {
        bugs: 0,
        features: 0
      };
      printData[pid].bugs = (totalBugs / totalProjectTime) * 100 + "";
      printData[pid].bugs = printData[pid].bugs.substr(0, 4);

      totalPerc += parseFloat(printData[pid].bugs.substr(0, 4));

      printData[pid].features = (totalFeatures / totalProjectTime) * 100 + "";
      printData[pid].features = printData[pid].features.substr(0, 4);

      totalPerc += parseFloat(printData[pid].features.substr(0, 4));

      console.log("total", totalPerc);
      if (100.0 - totalPerc < 1.0) {
        printData[pid].features =
          parseFloat(printData[pid].features) + (100.0 - totalPerc);
      }
    }
    console.log("full data", fullData);

    console.log(this.printData);
    this.tableRows = [];
    this.hiddenTable = [];
    this.hiddenTable.push(
      <tr>
        <td className="zeropad" align="center">
          <b>Name</b>
        </td>
        <td className="zeropad" align="center">
          <b>Salary</b>
        </td>
        <td className="zeropad" align="center">
          <b>Type</b>
        </td>
        <td className="zeropad" align="center">
          <b>Cost Center</b>
        </td>
        <td className="zeropad" align="center">
          <b>Allocation %</b>
        </td>
      </tr>
    );
    this.csvData.push([
      "Name",
      "Salary",
      "Type",
      "Cost Center",
      "Allocation %"
    ]);

    if (this.projectStats.length == 0) {
      this.props.addreport("del", [], this.sentOnce);
      this.sentOnce = true;
    }
    for (let pid in this.projectStats) {
      console.log("nantest", this.projectStats[pid].features);

      let wdDiv = {};
      let pdTd = {};
      if (this.props.showName) {
        wdDiv = {
          width: "300px",
          textAlign: "left"
        };
        pdTd = {
          paddingRight: "0px"
        };
      }
      console.log("pid#->", pid);
      let showName = this.props.ccdata;
      if (!showName) {
        showName = this.props.pnames[pid].name;
      } else {
        showName = this.props.ccdata[pid];
      }
      if (this.props.pnames[pid]) {
        this.tableRows.push(
          <tr key={pid}>
            <td style={pdTd} className="projectname ltd">
              <a href={this.props.pnames[pid].url}>
                <div style={wdDiv}>{showName}</div>
              </a>
            </td>
            <td className="bugperc rtd">{this.projectStats[pid].bugs}%</td>
            <td className="featperc rtd">{this.projectStats[pid].features}%</td>
            <td className="sharecenter">
              <div className="progress statsbar">
                <div
                  className="progress-bar progress-bar-striped pblue"
                  role="progressbar"
                  style={{ width: `${this.projectStats[pid].features}%` }}
                  aria-valuenow="15"
                  aria-valuemin="0"
                  aria-valuemax="100"
                />
                <div
                  className="progress-bar progress-bar-striped bg-danger"
                  role="progressbar"
                  style={{ width: `${this.projectStats[pid].bugs}%` }}
                  aria-valuenow="30"
                  aria-valuemin="0"
                  aria-valuemax="100"
                />
              </div>
            </td>
          </tr>
        );

        if (this.props.showName) {
          this.hiddenTable.push(
            <tr>
              <td>{this.props.name}</td>
              <td> </td>
              <td>Bug</td>
              <td>{this.props.ccdata[pid]}</td>
              <td>{printData[pid].bugs}</td>
            </tr>
          );
          this.hiddenTable.push(
            <tr>
              <td>{this.props.name}</td>
              <td> </td>
              <td>Feature</td>
              <td>{this.props.ccdata[pid]}</td>
              <td>{printData[pid].features}</td>
            </tr>
          );

          this.csvData.push([
            this.props.name,
            "",
            "Bug",
            this.props.ccdata[pid],
            printData[pid].bugs
          ]);
          this.csvData.push([
            this.props.name,
            "",
            "Feature",
            this.props.ccdata[pid],
            printData[pid].features
          ]);

          if (
            !this.sentArray[pid] &&
            this.props.ccloaded &&
            !(
              this.projectStats[pid].features == 0 &&
              this.projectStats[pid].bugs == 0
            )
          ) {
            this.props.addreport(
              "",
              [
                [
                  this.props.name,
                  "",
                  "Bug",
                  this.props.ccdata[pid],
                  printData[pid].bugs
                ],
                [
                  this.props.name,
                  "",
                  "Feature",
                  this.props.ccdata[pid],
                  printData[pid].features
                ]
              ],
              this.sentOnce
            );
            this.sentOnce = true;
            this.sentArray[pid] = true;
          }
        }
      } else {
        this.tableRows.push(
          <tr key={pid}>
            <td className="projectname">Project {pid}</td>
            <td className="bugperc">{this.projectStats[pid].bugs}%</td>
            <td className="featperc">{this.projectStats[pid].features}%</td>
          </tr>
        );

        if (this.props.showName) {
          this.hiddenTable.push(
            <tr>
              <td className="zeropad">{this.props.name}</td>
              <td className="zeropad"> </td>
              <td className="zeropad">Bug</td>
              <td className="zeropad">{this.props.ccdata[pid]}</td>
              <td className="zeropad">{printData[pid].bugs}</td>
            </tr>
          );
          this.hiddenTable.push(
            <tr>
              <td className="zeropad">{this.props.name}</td>
              <td className="zeropad"> </td>
              <td className="zeropad">Feature</td>
              <td>{this.props.ccdata[pid]}</td>
              <td className="zeropad">{printData[pid].features}</td>
            </tr>
          );

          this.csvData.push([
            this.props.name,
            "",
            "Bug",
            this.props.ccdata[pid],
            printData[pid].bugs
          ]);
          this.csvData.push([
            this.props.name,
            "",
            "Feature",
            this.props.ccdata[pid],
            printData[pid].features
          ]);
        }
      }
    }

    let nameHead;
    let wdClass = "";
    console.log("showname", this.props.showName);
    console.log("csv data", this.csvData);
    if (this.props.showName) {
      nameHead = (
        <tr>
          <th className="nameth" colSpan="4">
            <div className="namehead">
              <span className="namepush">{this.props.name}</span>

              <span className="copybutton">
                <i
                  class="far fa-copy padit"
                  onClick={evt => {
                    evt.preventDefault();
                    console.log("unique", this.props.unique);
                    if (document.selection) {
                      //IE
                      var range = document.body.createTextRange();
                      range.moveToElementText(
                        document.getElementById(
                          "popularity-" + this.props.unique
                        )
                      );
                      range.execCommand("Copy");
                      document.selection.empty();
                    } else if (window.getSelection) {
                      //others
                      window.getSelection().removeAllRanges();
                      var range = document.createRange();
                      console.log(
                        "popu",
                        document.getElementById(
                          "popularity-" + this.props.unique
                        )
                      );
                      range.selectNode(
                        document.getElementById(
                          "popularity-" + this.props.unique
                        )
                      );

                      console.log("range", range);
                      window.getSelection().addRange(range);

                      document.execCommand("copy");
                      console.log("selection", window.getSelection());

                      window.getSelection().removeAllRanges();
                    }
                  }}
                />
                <CSVLink
                  filename={`${this.props.name}-COST-REPORT.csv`}
                  data={this.csvData}
                >
                  <a>
                    <i class="fas fa-download padit" />
                  </a>
                </CSVLink>
              </span>
            </div>
          </th>
        </tr>
      );

      wdClass = "dstable";
    }
    console.log("trows>", this.tableRows);
    let wdDiv = {};
    if (this.props.showName) {
      wdDiv = {
        width: "204px",
        textAlign: "left"
      };
    }
    if (this.projectStats.length == 0) {
      return (
        <div className="statsdisplay">
          <table className={wdClass}>
            {nameHead}
            <tbody>
              <tr>
                <td colSpan="4" className="ctd">
                  No information recorded
                </td>
              </tr>
            </tbody>
          </table>
          <table id={"popularity-" + this.props.unique} className="hiddentable">
            {this.hiddenTable}
          </table>
        </div>
      );
    }
    return (
      <div className="statsdisplay">
        <table className={wdClass}>
          {nameHead}
          <tr>
            <th className="ltd">
              <div style={wdDiv}>Repo</div>
            </th>
            <th className="bughead rtd">Bugs</th>
            <th className="feathead rtd">Features</th>
            <th className="rtd">Ratio</th>
          </tr>
          <tbody>{this.tableRows}</tbody>
        </table>
        <table id={"popularity-" + this.props.unique} className="hiddentable">
          {this.hiddenTable}
        </table>
      </div>
    );
  }
}
