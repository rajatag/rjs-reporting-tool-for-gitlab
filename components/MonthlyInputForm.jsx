import React from "react";
import config from "../config";
import "../styles/global.css";

require("react-month-picker-input/dist/react-month-picker-input.css");
import Select from "react-select";
import "react-select/dist/react-select.css";
import DatePicker from "react-date-picker";

export default class MonthlyInputForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      month: new Date().getMonth() + 1,
      year: new Date().getFullYear(),
      users: [],
      loaded: false,
      usernames: [],
      name: "",
      listopen: false,
      dateinput: "",
      error: false,
      errorMsg: "",
      selectusers: [],
      dateval: new Date()
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleUsernameChange = this.handleUsernameChange.bind(this);
    this.handleMonthChange = this.handleMonthChange.bind(this);
    this.hadleYearChange = this.hadleYearChange.bind(this);
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.addUsername = this.addUsername.bind(this);
    this.ifExists = this.ifExists.bind(this);
    config.KEY = this.props.pkey;
    this.usernameinput = null;

    config.BASE = this.props.base;
  }
  componentDidMount() {
    this.setState({ users: this.props.users, loaded: true, error: false });
  }
  ifExists(username) {
    for (let user of this.state.users) {
      if (username === user.username) {
        return user.name;
      }
    }
    return "";
  }
  getNames() {
    let names = [];
    for (let username of this.state.usernames) {
      for (let user of this.state.users) {
        // console.log(this.state.username,user.username);
        if (username === user.username) {
          names.push(user.name);
        }
      }
    }
    return names;
  }
  getUserId() {
    for (let user of this.state.users) {
      // console.log(this.state.username,user.username);
      if (this.state.username === user.username) {
        return user.id;
      }
    }
  }

  // removeUsername(username) {
  //     let newUsernames = [...this.state.usernames];
  //     for (let u in newUsernames) {
  //         if (this.state.usernames[u] == username) {
  //             console.log("found");
  //             newUsernames.splice(u, 1);
  //             break;
  //         }
  //     }
  //     this.setState({
  //         usernames: newUsernames
  //     });

  // }
  getUserIds() {
    let ids = [];
    for (let username of this.state.usernames) {
      for (let user of this.state.users) {
        if (username === user.username) {
          ids.push(user.id);
        }
      }
    }
    return ids;
  }
  handleSubmit(evt) {
    evt.preventDefault();
    console.log(this.state);
    let okayLength = this.state.usernames.length;
    if (this.state.year && okayLength != 0) {
      this.props.history.push(
        `/monthly/${this.state.year}/${this.state.month}/${this.getUserIds.call(
          this
        )}`
      );
      if (this.props.loadagain) {
        location.reload();
      }
    } else {
      console.log("invalid");
    }
  }
  handleDateChange(evt) {
    console.log(evt.target.value);
    let monthDate = new Date(evt.target.value);
    this.setState({
      month: monthDate.getMonth() + 1
    });
  }
  handleUsernameChange(evt) {
    this.setState({ username: evt.target.value });
  }
  addUsername(username) {
    console.log("usrnmae", username);
    let name = this.ifExists(username);
    if (this.state.year && name) {
      for (let u of this.state.usernames) {
        if (u == username) {
          return;
        }
      }
      this.state.usernames.push(username);
      this.state.selectusers.push({
        value: username,
        label: username
      });
      this.setState({ username: "" });
    }
  }
  handleSelectChange(values) {
    let usernames = values.map(value => {
      return value.value;
    });

    let selVals = values.map(value => {
      return {
        value: value.value,
        label: value.value
      };
    });
    this.setState({ selectusers: selVals, usernames: usernames });
    // this.usernameinput.blur();
    // this.setState({ username: value }, () => {

    // });
  }
  handleMonthChange(evt) {
    console.log(evt.target.value);
    this.setState({ month: parseInt(evt.target.value) });
  }
  hadleYearChange(evt) {
    console.log(evt.target.value);
    this.setState({ year: parseInt(evt.target.value) });
  }
  render() {
    if (this.state.error) {
      return (
        <div className="formerror">
          ERROR<br />
          {this.state.errorMsg}
        </div>
      );
    }
    if (this.state.error) {
      return <div className="formerror">ERROR</div>;
    }
    if (!this.state.loaded) {
      return (
        <div className="inputform prgrs">
          <div className="progress">
            <div
              className="progress-bar progress-bar-striped progress-bar-animated"
              role="progressbar"
              aria-valuenow="100"
              aria-valuemin="0"
              aria-valuemax="100"
              style={{ width: "100%" }}
            />
          </div>
        </div>
      );
    } else {
      return (
        <div
          style={{
            marginTop: "5%"
          }}
          className="card"
        >
          <form onSubmit={this.handleSubmit}>
            <table className="noshadow">
              <tr className="noborder">
                <td className="noborder">Select users:</td>
                <td className="noborder">
                  <span className="floatr">
                    <Select
                      className="selectspan"
                      placeholder="Search by name"
                      multi={true}
                      name="usernames"
                      value={this.state.selectusers}
                      onChange={this.handleSelectChange}
                      options={(() => {
                        let opts = [];
                        for (let user of this.state.users) {
                          opts.push({
                            value: user.username,
                            label: user.name
                          });
                        }
                        return opts;
                      })()}
                    />
                  </span>
                </td>
              </tr>
              <tr className="noborder">
                <td className="noborder">Select month:</td>
                <td className="noborder">
                  <div className="floatr forcer">
                    <DatePicker
                      className="pickerclass"
                      onChange={date => {
                        this.setState({
                          month: date.getMonth() + 1,
                          year: date.getFullYear(),
                          dateval: date
                        });
                      }}
                      value={this.state.dateval}
                      maxDate={new Date()}
                      maxDetail="year"
                    />
                  </div>
                </td>
              </tr>
              <tr className="noborder">
                <td className="noborder" colSpan="2">
                  <input
                    type="submit"
                    className="btn submitter btn-primary"
                    value="Get Monthly Report"
                  />
                </td>
              </tr>
            </table>
          </form>
        </div>
      );
    }
  }
}
