import React, { Component } from "react";
import config from "../config";
import DatePicker from "react-date-picker";
import Select from "react-select";

export default class DateInputForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: "",
      month: 1,
      year: 2018,
      users: [],
      loaded: false,
      username: "",
      name: "",
      onlydate: 1,
      error: false,
      errorMsg: "",
      startDate: {},
      selectusers: [],
      usernames: []
    };
    config.KEY = this.props.pkey;
    config.BASE = this.props.base;
  }
  componentDidMount() {
    let yesterdate = new Date();
    yesterdate.setDate(yesterdate.getDate() - 1);
    let ymnth = yesterdate.getMonth() + 1;
    if (ymnth < 10) {
      ymnth = "0" + ymnth;
    }

    let yyear = yesterdate.getFullYear();

    if (yesterdate.getDay() == 0) {
      yesterdate.setDate(yesterdate.getDate() - 2);
    } else if (yesterdate.getDay() == 6) {
      yesterdate.setDate(yesterdate.getDate() - 1);
    }

    let yday = yesterdate.getDate();
    if (yday < 10) {
      yday = "0" + yday;
    }

    console.log("dayofweek", yesterdate.getDay());
    console.log("shoulddate cdm", `${yyear}-${ymnth}-${yday}`);
    this.setState(
      {
        users: this.props.users,
        loaded: true,
        date: `${yyear}-${ymnth}-${yday}`,
        startDate: yesterdate
      },
      () => {
        this.filterUsers.call(this);
      }
    );
  }
  getUserIds() {
    let ids = [];
    for (let username of this.state.usernames) {
      for (let user of this.state.users) {
        if (username === user.username) {
          ids.push(user.id);
        }
      }
    }
    return ids;
  }
  getNames() {
    let names = [];
    for (let user of this.state.users) {
      names.push(user.name);
    }
    return names;
  }
  filterUsers() {
    let usrs = this.state.users.filter(user => {
      let createdDate = new Date(user.created_at);
      createdDate.setMilliseconds(0);
      createdDate.setSeconds(0);
      createdDate.setHours(0);
      createdDate.setMinutes(0);

      let nowDate = new Date(this.state.date);
      nowDate.setMilliseconds(0);
      nowDate.setSeconds(0);
      nowDate.setHours(0);
      nowDate.setMinutes(0);

      console.log("nowcreate", nowDate, createdDate);
      console.log("nowcreate", nowDate.getTime(), createdDate.getTime());
      if (nowDate.getTime() < createdDate.getTime()) {
        return false;
      } else {
        return true;
      }
    });
    console.log(usrs);
    this.setState({ users: usrs });
  }
  handleChange(val) {
    this.setState({ startDate: val });
    console.log("datechange", val);
  }
  handleSelectChange(values) {
    let usernames = values.map(value => {
      return value.value;
    });

    let selVals = values.map(value => {
      return {
        value: value.value,
        label: value.value
      };
    });
    this.setState({ selectusers: selVals, usernames: usernames });
  }
  render() {
    if (this.state.error) {
      return (
        <div className="formerror">
          ERROR<br />
          {this.state.errorMsg}
        </div>
      );
    }
    let nowDate = new Date(Date.now());
    let mnth = nowDate.getMonth();
    let yr = nowDate.getFullYear();
    let dte = nowDate.getDate();

    mnth++;
    if (mnth < 10) {
      mnth = "0" + mnth;
    }

    if (dte < 10) {
      dte = "0" + dte;
    }
    console.log("limdate", `${yr}-${mnth}-${dte}`);
    if (!this.state.loaded) {
      return (
        <div className="inputform prgrs">
          <div className="progress">
            <div
              className="progress-bar progress-bar-striped progress-bar-animated"
              role="progressbar"
              aria-valuenow="100"
              aria-valuemin="0"
              aria-valuemax="100"
              style={{ width: "100%" }}
            />
          </div>
        </div>
      );
    } else {
      return (
        <div
          style={{
            marginTop: "5%"
          }}
          className="card"
        >
          <table className="noshadow">
            <tr className="noborder">
              <td className="noborder">Select users:</td>
              <td className="noborder">
                <span className="floatr">
                  <Select
                    className="selectspan"
                    placeholder="Search by name"
                    multi={true}
                    name="usernames"
                    value={this.state.selectusers}
                    onChange={this.handleSelectChange.bind(this)}
                    options={(() => {
                      let opts = [];
                      for (let user of this.state.users) {
                        opts.push({
                          value: user.username,
                          label: user.name
                        });
                      }
                      return opts;
                    })()}
                  />
                </span>
              </td>
            </tr>

            <tr className="noborder">
              <td className="noborder">Select date:</td>
              <td className="noborder">
                <div className="floatr forcer">
                  <DatePicker
                    className="pickerclass"
                    onChange={this.handleDateChange.bind(this)}
                    value={this.state.startDate}
                    maxDate={new Date()}
                  />
                </div>
              </td>
            </tr>
            <tr className="noborder">
              <td colSpan="2" className="noborder">
                <button
                  onClick={this.handleDateSubmit.bind(this)}
                  className="btn submitter btn-primary"
                >
                  Get Daily Report
                </button>
              </td>
            </tr>
            <tr className="noborder">
              <td colSpan="2" className="noborder">
                <button
                  onClick={this.handleListSubmit.bind(this)}
                  className="btn submitter btn-primary"
                >
                  Get Issue List
                </button>
              </td>
            </tr>
          </table>
        </div>
      );
    }
  }
  handleDateChange(value) {
    let setdate = `${value.getFullYear()}-${value.getMonth() +
      1}-${value.getDate()}`;
    console.log("dateset", setdate);
    this.setState({ date: setdate, startDate: value }, () => {
      this.filterUsers.call(this);
    });
  }
  handleDateSubmit(evt) {
    console.log("shoulddate", this.state.date);
    let dateArray = this.state.date.split("-").map(item => parseInt(item));

    console.log("datearray", dateArray);
    let usrString = this.getUserIds(this.state.usernames).join(",");

    if (this.state.usernames.length == 0) {
      this.setState(
        { month: dateArray[1], year: dateArray[0], onlydate: dateArray[2] },
        () => {
          this.props.history.push(
            `/daily/${this.state.year}/${this.state.month}/${
              this.state.onlydate
            }`
          );
          if (this.props.loadagain) {
            location.reload();
          }
          return;
        }
      );
      return;
    }

    this.setState(
      { month: dateArray[1], year: dateArray[0], onlydate: dateArray[2] },
      () => {
        this.props.history.push(
          `/daily/${this.state.year}/${this.state.month}/${
            this.state.onlydate
          }/${usrString}`
        );
        if (this.props.loadagain) {
          location.reload();
        }
      }
    );
  }
  handleListSubmit(evt) {
    console.log("shoulddate", this.state.date);
    let dateArray = this.state.date.split("-").map(item => parseInt(item));

    console.log("datearray", dateArray);
    let usrString = this.getUserIds(this.state.usernames).join(",");

    if (this.state.usernames.length == 0) {
      this.setState(
        { month: dateArray[1], year: dateArray[0], onlydate: dateArray[2] },
        () => {
          console.log(
            "historypush",
            `/daily/${this.state.year}/${this.state.month}/${
              this.state.onlydate
            }/?issuelist=true`
          );
          this.props.history.push(
            `/daily/${this.state.year}/${this.state.month}/${
              this.state.onlydate
            }/?issuelist=true`
          );
          if (this.props.loadagain) {
            location.reload();
          }
        }
      );
      return;
    }

    this.setState(
      { month: dateArray[1], year: dateArray[0], onlydate: dateArray[2] },
      () => {
        this.props.history.push(
          `/daily/${this.state.year}/${this.state.month}/${
            this.state.onlydate
          }/${usrString}/?issuelist=true`
        );
        if (this.props.loadagain) {
          location.reload();
        }
      }
    );
  }
}
