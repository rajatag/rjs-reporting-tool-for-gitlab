import React, { Component } from "react";
import config from "../config";
import Select from "react-select";
import DatePicker from "react-date-picker";
import { Link } from "react-router-dom";
export default class CCInputForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: "",
      month: new Date().getMonth() + 1,
      year: new Date().getFullYear(),
      users: [],
      loaded: false,
      username: "",
      name: "",
      onlydate: 1,
      error: false,
      errorMsg: "",
      dateval: new Date(),
      selectusers: [],
      usernames: []
    };
    config.KEY = this.props.pkey;
    config.BASE = this.props.base;
  }
  componentDidMount() {
    this.setState({ users: this.props.users, loaded: true });
  }
  getUserIds() {
    let ids = [];
    for (let username of this.state.usernames) {
      for (let user of this.state.users) {
        if (username === user.username) {
          ids.push(user.id);
        }
      }
    }
    return ids;
  }
  getNames() {
    let names = [];
    for (let user of this.state.users) {
      names.push(user.name);
    }
    return names;
  }
  handleSelectChange(values) {
    let usernames = values.map(value => {
      return value.value;
    });

    let selVals = values.map(value => {
      return {
        value: value.value,
        label: value.value
      };
    });
    this.setState({ selectusers: selVals, usernames: usernames });
  }
  render() {
    console.log("render called");
    if (this.state.error) {
      return (
        <div className="formerror">
          ERROR<br />
          {this.state.errorMsg}
        </div>
      );
    }
    if (!this.state.loaded) {
      return (
        <div className="inputform prgrs">
          <div className="progress">
            <div
              className="progress-bar progress-bar-striped progress-bar-animated"
              role="progressbar"
              aria-valuenow="100"
              aria-valuemin="0"
              aria-valuemax="100"
              style={{ width: "100%" }}
            />
          </div>
        </div>
      );
    } else {
      return (
        <div
          style={{
            marginTop: "5%"
          }}
          className="card"
        >
          <table className="noborder noshadow">
            <tr className="noborder">
              <td className="noborder">Select users:</td>
              <td className="noborder">
                <span className="floatr">
                  <Select
                    className="selectspan"
                    placeholder="Search by name"
                    multi={true}
                    name="usernames"
                    value={this.state.selectusers}
                    onChange={this.handleSelectChange.bind(this)}
                    options={(() => {
                      let opts = [];
                      for (let user of this.state.users) {
                        opts.push({
                          value: user.username,
                          label: user.name
                        });
                      }
                      return opts;
                    })()}
                  />
                </span>
              </td>
            </tr>
            <tr className="noborder">
              <td className="noborder">Select month:</td>
              <td className="noborder">
                <div className="floatr forcer">
                  <DatePicker
                    className="pickerclass"
                    onChange={date => {
                      this.setState({
                        month: date.getMonth() + 1,
                        year: date.getFullYear(),
                        dateval: date
                      });
                    }}
                    value={this.state.dateval}
                    maxDate={new Date()}
                    maxDetail="year"
                  />
                </div>
              </td>
            </tr>

            <tr className="noborder">
              <td className="noborder" align="center" colSpan="2">
                <button
                  onClick={this.handleDateSubmit.bind(this)}
                  className="btn submitter btn-primary"
                >
                  Get CC Report
                </button>
              </td>
            </tr>
            <tr>
              <td className="noborder" colSpan="2">
                <div className="dblink">
                  <Link to="/dbmanager">
                    <a className="linkdb">Manage CC Database</a>
                  </Link>
                </div>
              </td>
            </tr>
          </table>
        </div>
      );
    }
  }
  handleMonthChange(evt) {
    this.setState({ month: parseInt(evt.target.value) });
  }
  handleYearChange(evt) {
    this.setState({ year: parseInt(evt.target.value) });
  }
  handleDateChange(evt) {
    console.log("gotdate", evt.target.value);
    this.setState({ date: evt.target.value });
  }
  handleDateSubmit(evt) {
    console.log("push");
    console.log(this.props.history);
    this.props.history.push(`/cc/${this.state.year}/${this.state.month}`);
    if (this.props.loadagain) {
      location.reload();
    }
  }

  handleDateSubmit(evt) {
    console.log("shoulddate", this.state.date);
    let dateArray = this.state.date.split("-").map(item => parseInt(item));

    console.log("datearray", dateArray);
    let usrString = this.getUserIds(this.state.usernames).join(",");

    if (this.state.usernames.length == 0) {
      this.props.history.push(`/cc/${this.state.year}/${this.state.month}`);
      if (this.props.loadagain) {
        location.reload();
      }
      return;
    }

    this.props.history.push(
      `/cc/${this.state.year}/${this.state.month}/${usrString}`
    );
    if (this.props.loadagain) {
      location.reload();
    }
    return;
  }
}
